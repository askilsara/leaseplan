package starter.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import starter.api.CarsAPI;

public class CommonStepDefinitions {
    
    @Steps
    private CarsAPI carsAPI = new CarsAPI();


    @Given("the host is {string}")
    public void theHostIs(String arg0) {
        carsAPI.setBasedUrl(arg0);
    }

    @Then("the response code is {int}")
    public void theResponseCodeIs(int arg0) {
        carsAPI.checkStatus(arg0);
    }


}
