package starter.stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;
import starter.api.SearchAPI;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


public class SearchStepDefinitions {

    @Steps
    private SearchAPI searchAPI = new SearchAPI();

    @When("the user calls search endpoint for {string}")
    public void heCallsEndpointSearchEndpoint(String arg0) {
        searchAPI.get(arg0);
    }

    @Then("{string} fields contains {string}")
    public void heSeesTheResultsDisplayedFor(String arg0, String arg1) {
        searchAPI.checkResponseContains(arg0, arg1);
    }

    @Then("he sees error message")
    public void heDoesNotSeeTheResults() {
        searchAPI.checkErrorMessage();
    }


    @Then("every object has expected field and type:")
    public void everyObjectContainsExpectedValues(List<Map<String, String>> objects) throws ClassNotFoundException {
        searchAPI.checkSearchObject(objects.get(0));
    }

    @And("{string} are not empty")
    public void fieldsAreNotEmpty(String arg0) {
        List<String> list = Arrays.asList(arg0.split(","));
        for (String field : list)
            searchAPI.checkRequiredField(field);
    }

    @When("the user calls search {string} endpoint for {string}")
    public void heCallsSearchRequests(String arg0, String arg1) {
        switch(arg0.toLowerCase()){
            case "get": searchAPI.get(arg1); break;
            case "post": searchAPI.post(arg1); break;
            case "put": searchAPI.put(arg1); break;
            case "patch": searchAPI.patch(arg1); break;
            case "delete": searchAPI.delete(arg1); break;
            default: Assert.fail("Unknown request type");
        }
        
    }
}
