package starter.custom.matchers;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ContainsIgnoringCaseMatcher extends TypeSafeMatcher<String> {
    private final String subString;
    private static final Logger logger = LoggerFactory.getLogger(ContainsIgnoringCaseMatcher.class);

    @Factory
    public static Matcher<String> containsIgnoringCase(final String subString) {
        return new ContainsIgnoringCaseMatcher(subString);
    }
    
    private ContainsIgnoringCaseMatcher(final String subString) {
        this.subString = subString;
    }

    @Override
    protected boolean matchesSafely(final String actualString) {
        boolean contains = actualString.toLowerCase().contains(this.subString.toLowerCase());
        if (!contains)
            logger.error("[" + actualString + "] doesn't contain expected substring [" + subString + "]");
        return contains;
    }

    @Override
    public void describeTo(final Description description) {
        description.appendText("containing substring \"" + subString + "\"");
    }
    
}
