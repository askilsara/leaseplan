package starter.custom.matchers;

import org.hamcrest.Description;
import org.hamcrest.DiagnosingMatcher;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class InstanceOfOrNullMatcher extends DiagnosingMatcher<Object> {
    private static final Logger logger = LoggerFactory.getLogger(InstanceOfOrNullMatcher.class);
    private final Class<?> expectedClass;
    private final Class<?> matchableClass;

    private InstanceOfOrNullMatcher(Class<?> expectedClass) {
        this.expectedClass = expectedClass;
        this.matchableClass = matchableClass(expectedClass);
    }

    private static Class<?> matchableClass(Class<?> expectedClass) {
        if (Boolean.TYPE.equals(expectedClass)) {
            return Boolean.class;
        } else if (Byte.TYPE.equals(expectedClass)) {
            return Byte.class;
        } else if (Character.TYPE.equals(expectedClass)) {
            return Character.class;
        } else if (Double.TYPE.equals(expectedClass)) {
            return Double.class;
        } else if (Float.TYPE.equals(expectedClass)) {
            return Float.class;
        } else if (Integer.TYPE.equals(expectedClass)) {
            return Integer.class;
        } else if (Long.TYPE.equals(expectedClass)) {
            return Long.class;
        } else {
            return Short.TYPE.equals(expectedClass) ? Short.class : expectedClass;
        }
    }

    protected boolean matches(Object item, Description mismatch) {
        if (null != item) {
            if (!this.matchableClass.isInstance(item)) {
                System.out.println(item.getClass());
                mismatch.appendValue(item).appendText(" is a " + item.getClass().getName());
                return false;
            } else {
                return true;
            }
        } else {
            mismatch.appendText("null");
            return true;
        }
    }

    public void describeTo(Description description) {
        description.appendText("an instance of ").appendText(this.expectedClass.getName());
    }

    @Factory
    public static Matcher<Object> instanceOfOrNull(Class<?> type) {
        return new InstanceOfOrNullMatcher(type);
    }

}
