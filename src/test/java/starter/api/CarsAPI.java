package starter.api;

import io.restassured.RestAssured;
import net.thucydides.core.annotations.Step;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;

public class CarsAPI {

    @Step
    public void setBasedUrl(String arg0) {
        RestAssured.baseURI = arg0;
    }

    @Step
    public void checkStatus(int arg0) {
        restAssuredThat(response -> response.statusCode(arg0));
    }

}
