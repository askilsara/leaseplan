package starter.api;

import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import java.util.Map;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.*;
import static starter.custom.matchers.ContainsIgnoringCaseMatcher.containsIgnoringCase;
import static starter.custom.matchers.InstanceOfOrNullMatcher.instanceOfOrNull;

public class SearchAPI extends CarsAPI {
    private final String path = "/api/v1/search/test/{product}";
    private Response response;

    @Step
    public void checkSearchObject(Map<String, String> map) throws ClassNotFoundException {
        for (String s : map.keySet()) {
            Class<?> classType = Class.forName("java.lang." + map.get(s));
            restAssuredThat(response -> response.body(s, everyItem(instanceOfOrNull(classType))));
        }
    }

    @Step
    public void checkRequiredField(String field) {
        restAssuredThat(response -> response.body(field, everyItem(notNullValue())));
    }


    @Step
    public void checkResponseContains(String field, String subString) {
        restAssuredThat(response -> response.body(field, everyItem(containsIgnoringCase(subString))));
    }


    @Step
    public void checkErrorMessage() {
        restAssuredThat(response -> response.body("detail.error", equalTo(true)));
    }


    @Step
    public void get(String arg0) {
        response = SerenityRest.given().pathParam("product", arg0).get(path);
    }


    @Step
    public void post(String arg0) {
        response = SerenityRest.given().pathParam("product", arg0).post(path);
    }


    @Step
    public void put(String arg0) {
        response = SerenityRest.given().pathParam("product", arg0).put(path);
    }


    @Step
    public void patch(String arg0) {
        response = SerenityRest.given().pathParam("product", arg0).patch(path);
    }


    @Step
    public void delete(String arg0) {
        response = SerenityRest.given().pathParam("product", arg0).delete(path);
    }
}
