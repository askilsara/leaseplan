Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/test/{product} for getting the products.
### Available products: "apple", "mango", "tofu", "water"
### Prepare Positive and negative scenarios
  Background:
    Given the host is "https://waarkoop-server.herokuapp.com"

  @positive
  Scenario Outline: Check search results
    When the user calls search endpoint for "<parameter>"
    Then the response code is 200
    And "title" fields contains "<parameter>"
    Examples:
      | parameter |
      | mango     |
      | apple     |
      | tofu      |
      | water     |

  @positive
  Scenario Outline: Check field types
    When the user calls search endpoint for "<parameter>"
    Then every object has expected field and type:
      | provider | title  | url    | brand  | price  | unit   | isPromo | promoDetails | image  |
      | String   | String | String | String | Number | String | Boolean | String       | String |
    And "provider,title,url" are not empty
    Examples:
      | parameter |
      | mango     |
      | apple     |
      | tofu      |
      | water     |


  @negative
  Scenario: The product doesn't exist
    When the user calls search endpoint for "car"
    Then the response code is 404
    And he sees error message


  @negative
  Scenario: Empty request
    When the user calls search endpoint for ""
    Then the response code is 401


  @negative
  Scenario Outline: Invalid request type
    When the user calls search "<type>" endpoint for "mango"
    Then the response code is 405
    Examples:
      | type   |
      | POST   |
      | PUT    |
      | PATCH  |
      | DELETE |
  
    
