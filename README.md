Read me please before starting work with the project!

1. Install

    1.1. Install Java(you can download Java 1.8 from https://www.oracle.com/in/java/technologies/javase-downloads.html)
    1.2. Install IDE(I'm using IntelliJ IDEA community edition, but you can use other IDE)
    1.3. Install Gherkin and Cucumber plugins in your IDE if you need

2. Run

    2.1. Run tests with Maven: use "mvn clean install verify" command to run our scripts
    2.2 Run tests with class runner: run src/test/java/starter/TestRunner.java

3. Write new scripts

    3.1. You can find all scenarios in src/test/resources/features directory.
    3.2. Create a new .feature file or open already existed file
    3.3. Write a new scenario using Gherkin syntax https://cucumber.io/docs/gherkin/reference/
    3.4. You can use already implemented methods or write new steps
    3.5. All step definitions are located in src/test/java/starter/stepdefinitions
    3.6. CommonStepDefinitions contains global methods for all API requests
    3.7. Create a new class or extend already existed step definitions with new steps
    3.8. Create a new class(extends CarsAPI) to implement new api methods
    3.9. You can call new API methods in your step definitions class
    3.10. If you want to work with JSON objects and different entities, you can create a new class in src/test/java/starter/api/entities
  
  
  
  What's new(after refactoring):
  1. Added new scripts in src/test/resources/features/search/post_product.feature
  2. Added CommonStepDefinitions for global steps(methods that can be used for any endpoint)
  3. Added SearchApi class with methods for search endpoint(separate class should be created for any endpoint)
  4. Added custom matchers in src/test/java/starter/custom/matchers(needed to use them in some scripts)
  5. Added global api methods to parent class(CarsAPI)
  6. Updated already existed steps and moved methods from then to API classes
  7. Added the entity class(Car.java can be used and extended for working with JSON objects from search  and other endpoints)
  8. Added .gitlab-ci.yml and configured CI pipeline
  9. Added HTML reporting and test results to CI jobs
  
  
Thank you for your attention and have a nice day!